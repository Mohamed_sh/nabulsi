import { RequestBuilder, RequestMethod } from "./RequestBuilder";
import { ApiResponse } from "./ApiResponse";

export namespace Controllers {

    export class Categories {
        public static get(id: number) {
            return new RequestBuilder()
                .setUrl('/getCategories.php')
                .addParam('id', id)
                .setMethod(RequestMethod.GET)
                .execute<ApiResponse>();
        }
    }
    export class CategoriesById {
        public static get(id: number) {
            return new RequestBuilder()
                .setUrl('/getTopic.php')
                .addParam('id', id)
                .setMethod(RequestMethod.GET)
                .execute<ApiResponse>();
        }
    }

    export class Wisdom {
        public static get() {
            return new RequestBuilder()
                .setUrl('/getTopWisdomList.php')
                .setMethod(RequestMethod.GET)
                .execute<ApiResponse>();
        }
        public static getSingle() {
            return new RequestBuilder()
                .setUrl('/getSingleWisdomNew.php')
                .addParam("id", 0)
                .setMethod(RequestMethod.GET)
                .execute<ApiResponse>();
        }
    }

    export class New {
        public static get() {
            return new RequestBuilder()
                .setUrl('/getNewAlert.php')
                .setMethod(RequestMethod.GET)
                .execute<ApiResponse>();
        }
    }

    export class Questions {
        public static get(id: number) {
            return new RequestBuilder()
                .setUrl('/getByCatPaging.php')
                .setMethod(RequestMethod.POST)
                .addParam("category", id)
                .addParam("column", "all")
                .addParam("i_page", 1)
                .addParam("i_size", 15)
                .execute<ApiResponse>();
        }
    }

    export class SubCategory {
        public static get(id: number) {
            return new RequestBuilder()
                .setUrl('/getTopWisdom.php')
                .setMethod(RequestMethod.GET)
                .addParam("id", id)
                .execute<ApiResponse>();
        }
    }
}


// export const WisdomGetSingle = () => {
//     console.log("single wisdom");

//     return new RequestBuilder()
//         .setUrl('/getSingleWisdomNew.php')
//         .addParam("id", 0)
//         .setMethod(RequestMethod.GET)
//         .execute<ApiResponse>();

// }
