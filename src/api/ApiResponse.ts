import {AxiosPromise} from "axios";
import {RequestBuilder, RequestMethod} from "./RequestBuilder";
import Category from "../models/Category";


export class ApiResponseStatus {
    public success?: boolean;
    public code?: number;
    public message?: string;
}

export class ApiResponsePagination {
    public i_current_page?: number;
    public i_per_page?: number;
    public i_total_pages?: number;
    public i_total_objects?: number;
    public i_items_on_page?: number;
}

export class ApiResponse {
    public status?: ApiResponseStatus;
    public pagination?: ApiResponsePagination;
    public categoryList?: Array<Category>;
}

// export { ApiResponse, ApiResponseStatus };