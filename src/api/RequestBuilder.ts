import axios, {
    AxiosRequestConfig,
    AxiosTransformer,
    AxiosAdapter,
    AxiosBasicCredentials,
    AxiosProxyConfig,
    CancelToken,
    CancelTokenSource,
    AxiosPromise,
    AxiosResponse
} from "axios";
import { RequestManager } from "./RequestManager";
import Sugar from 'sugar';
import qs from 'qs';

export type RequestTransformerFunc = (data: any, headers: any) => void;
export type ResponseTransformerFunc = (data: any) => void;
export type OnUploadProgressFunc = (progressEvent: any) => void;
export type OnDownloadProgressFunc = (progressEvent: any) => void;


export enum RequestMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE'
}

export interface BaseResponse<T = any> extends AxiosResponse<T> {
    config: BaseRequestConfig;
}

export interface AxiosPromise2<T = any> extends Promise<BaseResponse<T>> {
}

export class BaseRequestConfig implements AxiosRequestConfig {
    // lib attr
    url?: string;
    method?: string;
    baseURL?: string;
    transformRequest?: AxiosTransformer;
    transformResponse?: AxiosTransformer;
    headers?: any;
    params?: any;
    paramsSerializer?: (params: any) => string;
    data?: any;
    timeout?: number;
    withCredentials?: boolean;
    adapter?: AxiosAdapter;
    auth?: AxiosBasicCredentials;
    responseType?: string;
    xsrfCookieName?: string;
    xsrfHeaderName?: string;
    onUploadProgress?: (progressEvent: any) => void;
    onDownloadProgress?: (progressEvent: any) => void;
    maxContentLength?: number;
    validateStatus?: (status: number) => boolean;
    maxRedirects?: number;
    httpAgent?: any;
    httpsAgent?: any;
    proxy?: AxiosProxyConfig | false;
    cancelToken?: CancelToken;

    // new attr
    responseKey?: string;
    responseClass?: string;

    constructor() {
    }
}



// TODO: add query string support
export class RequestBuilder {

    protected _url?: string;
    protected _baseURL?: string;
    protected _method: RequestMethod = RequestMethod.GET;
    protected _headers = new Object();
    protected _params = new Object();
    protected _formData = new FormData();
    protected _requestTransformers = Array<RequestTransformerFunc>();
    protected _responseTransformers = Array<ResponseTransformerFunc>();
    protected _onUploadProgress?: OnUploadProgressFunc;
    protected _onDownloadProgress?: OnDownloadProgressFunc;
    protected _cancelToken?: CancelTokenSource;

    constructor() {

        this._cancelToken = axios.CancelToken.source();
    }

    public setBaseUrl(baseUrl: string) {
        this._baseURL = baseUrl;
        return this;
    }

    public get baseURL() {
        return this._baseURL;
    }

    public set url(url: string) {
        this._url = url;
    }

    public setUrl(url: string) {
        this._url = url;
        return this;
    }

    public get method() {
        return this._method;
    }

    public set method(method: RequestMethod) {
        this._method = method;
    }

    public setMethod(method: RequestMethod) {
        this._method = method;
        return this;
    }

    public get headers() {
        return this._headers;
    }

    public set headers(headers: any) {
        this._headers = headers;
    }

    public addHeader(keyOrObject: string, value: string) {
        this._headers[keyOrObject] = value;
        return this;
    }

    public removeHeader(key: string) {
        Sugar.Object.reject(this._headers, key);
        return this;
    }

    public get params() {
        return this._params;
    }

    public set setParams(params: any) {
        this._params = params;
    }

    public setParam(params: any) {
        this._params = params;
        return this;
    }

    public addParam(key: string, value: any = null) {
        this._params[key] = value;
        return this;
    }


    public addFormData(key: string, value: any = null) {
        this._formData.append(key, value);
        return this;
    }

    public removeParam(key: string) {
        Sugar.Object.reject(this._params, key);
        return this;
    }

    public addRequestTransformer(transformer: RequestTransformerFunc) {
        Sugar.Array.append(this._requestTransformers, transformer);
        return this;
    }

    public removeRequestTransformer(transformer: RequestTransformerFunc) {
        Sugar.Array.remove(this._requestTransformers, transformer);
        return this;
    }

    public clearRequestTransformers() {
        Sugar.Array.removeAt(this._requestTransformers, 0, this._requestTransformers.length);
        return this;
    }

    public addResponseTransformer(transformer: ResponseTransformerFunc) {
        Sugar.Array.append(this._responseTransformers, transformer);
        return this;
    }

    public removeResponseTransformer(transformer: ResponseTransformerFunc) {
        Sugar.Array.remove(this._responseTransformers, transformer);
        return this;
    }

    public clearResponseTransformers() {
        Sugar.Array.removeAt(this._responseTransformers, 0, this._responseTransformers.length);
        return this;
    }

    public setOnUploadProgress(func: OnUploadProgressFunc) {
        this._onUploadProgress = func;
        return this;
    }

    public unsetOnUploadProgress() {
        this._onUploadProgress = undefined;
        return this;
    }

    public setOnDownloadProgress(func: OnDownloadProgressFunc) {
        this._onDownloadProgress = func;
        return this;
    }

    public unsetOnDownloadProgress() {
        this._onDownloadProgress = undefined;
        return this;
    }

    public execute<T>(): AxiosPromise<T> {
        var config: BaseRequestConfig = new BaseRequestConfig();
        config.url = this._url;
        config.method = this._method.toString();
        config.headers = this._headers;
        if (this._formData['_parts'].length !== 0) {
            config.data = this._formData
        }
        else {
            switch (this._method) {
                case RequestMethod.GET:
                case RequestMethod.DELETE:
                    config.params = this._params;
                    break;
                case RequestMethod.POST:
                    config.data = qs.stringify(this._params);
                    break;
                case RequestMethod.PUT:
                    config.data = this._params;
                    break;
            }
        }
        // config.transformRequest = this._requestTransformers;
        // config.transformResponse = this._responseTransformers;
        config.onUploadProgress = this._onUploadProgress;
        config.onDownloadProgress = this._onDownloadProgress;
        config.cancelToken = this._cancelToken!.token;

        var request = RequestManager.shared.axios.request<T>(config);

        return request;
    }
}