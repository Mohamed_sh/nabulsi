import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  FlatList,
  View
} from 'react-native'
import { observer } from "mobx-react";
import Axios from "axios";
import ScreenComponent from "../_base/screen-component";
import SideMenuScreen from "../side-menu/SideMenuScreen";
import { Navigation } from "react-native-navigation";
import MainScreen from "./MainScreen";
const list = [1, 2, 3, 4, 5]


interface Props {
  navigator: any
}

interface State {

}

@observer
export default class Splash extends ScreenComponent<Props, State> {

  static screenID = "Splash";
  static push = () => ScreenComponent.navigator.push({ screen: Splash.screenID });

  _drawer: any;
  componentDidMount() {
    setTimeout(() => {
      Navigation.startSingleScreenApp({
        screen: {
          screen: MainScreen.screenID,
        },
        drawer: {
          right: {
            screen: SideMenuScreen.screenID,
          },
          style: {
            // leftDrawerWidth: 50, // optional, add this if you want a define left drawer width (50=percent)
            // rightDrawerWidth: 50, // optional, add this if you want a define right drawer width (50=percent)
          },
          // type: 'TheSideBar',
          // animationType: 'facebook',
          // disableOpenGesture: true
        },
        appStyle: {
          orientation: 'portrait'
        },
      });
    }, 100);
  }

  render() {

    return (
      <View style={{ flex: 1, alignSelf: 'stretch', alignContent: 'center' }}>
        <Text>
          Splash
        </Text>
      </View>
    )
  }
}