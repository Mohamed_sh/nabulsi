/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import ScreenComponent from "../_base/screen-component";
import Header from "../../components/general/Header";
import WisdomSection from "../../components/main/WisdomSection";
// import CategoryListView from "../../components/main/CategoryListView";
// import CategoryScreen from "../category/CategoryScreen";
import PatternBackground from "../../components/general/PatternBackground";
import Search from "../../components/general/Search";
import MainStore from "../../stores/MainStore";
import { observer } from "mobx-react";
import DrawerComponent from "../../components/general/DrawerComponent";
import Axios from "axios";

interface Props {
  navigator: any
}


interface State {

}

@observer
export default class MainScreen extends ScreenComponent<Props, State> {
  static screenID = "MainScreen";
  static screenTitle = "موسوعة النابلسي للعلوم الإسلامية";
  static push = () => ScreenComponent.navigator.push({ screen: MainScreen.screenID });

  _drawer: any;

  componentWillMount() {
    ScreenComponent.navigator = this.props.navigator;
  }

  async componentDidMount() {
    // MainStore.getCategoryRequest()
    const res = await Axios.get("http://alhudagroup-tr.com/API/getSingleWisdomNew.php", {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    }, )

    console.log("res", res);

    MainStore.getWisdomRequest()
    ScreenComponent.drawer = this._drawer;

  }

  // onListViewCellClicked = (categoryID: number, categoryName: string) => {
  //   CategoryScreen.push(categoryID, categoryName);
  // };



  render() {
    return (
      <DrawerComponent>
        <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
          <PatternBackground />
          <Header title={MainScreen.screenTitle} />
          <View style={{ alignItems: 'center' }}>
            <Search />
          </View>
          <WisdomSection wisdom={MainStore.getWisdom} />
          {/* <CategoryListView
                        data={MainStore.share.getCategory}
                        onListViewCellClicked={this.onListViewCellClicked}
                    /> */}
        </ScrollView>
      </DrawerComponent>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});
