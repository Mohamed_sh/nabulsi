import React, { Component } from 'react'
// import RNProgressHUB from 'react-native-progresshub';
// import { RNToasty } from 'react-native-toasty'
// import AppConstant from "../../constant/Constant";
import { ActivityIndicator, View } from "react-native";

interface Props {

}
interface State {
}

class ScreenComponent<State, Props> extends Component<State, Props> {

    public static navigator: any;
    public static drawer: any;

    public static navigatorStyle = {
        navBarHidden: true,
        navBarBackgroundColor: "white"
    };
    public static pop = () => {
        ScreenComponent.navigator.pop()
    };
    public static dismissModal = () => {
        ScreenComponent.navigator.dismissModal()
    };

    public static closeDrawer = () => {
        ScreenComponent.navigator.toggleDrawer({
            side: 'right',
            to: 'close'
        });
    };

    public static toggleDrawer = () => {
        ScreenComponent.navigator.toggleDrawer({
            side: 'right',
            animated: true,
        });
    };

    public static resetToRoot = () => {
        ScreenComponent.navigator.resetTo({ screen: "MainScreen" });
    };


    public static popToRoot = () => {
        ScreenComponent.navigator.popToRoot();
    };

    public static showActivityIndicator() {
        // RNProgressHUB.showSpinIndeterminate();
        <View style={{ flex: 1, alignSelf: 'stretch', alignContent: 'center' }}>
            <ActivityIndicator size={12} color="red" />
        </View>
    };

    public static hideActivityIndicator() {
        <View style={{ flex: 1, alignSelf: 'stretch', alignContent: 'center' }} />
    };


    // public static showToast(message: string) {
    //     RNToasty.Normal({title: message});
    // };

}

export default ScreenComponent;