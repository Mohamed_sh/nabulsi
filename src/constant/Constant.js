"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Color;
(function (Color) {
    Color["Blue"] = "#42A5F5";
    Color["BlueDark"] = "#021B47";
    Color["Purple"] = "#B388FF";
    Color["Pink"] = "#F06292";
    Color["White"] = "#FFF";
    Color["Black"] = "#000";
    Color["Gray"] = "#C4C4C4";
    Color["GrayDark"] = "#A1A8B6";
    Color["GrayLight"] = "#E8E8E8";
    Color["GrayWhite"] = "#F5F5F5";
})(Color || (Color = {}));
var Font;
(function (Font) {
    Font["KanunReqular"] = "KanunAR+LT-Regular";
    Font["KanunLight"] = "KanunAR+LT-Light";
})(Font || (Font = {}));
var SelectedCategory;
(function (SelectedCategory) {
    SelectedCategory[SelectedCategory["YouTube"] = 0] = "YouTube";
    SelectedCategory[SelectedCategory["SoundCloud"] = 1] = "SoundCloud";
})(SelectedCategory || (SelectedCategory = {}));
var AppConstant = { Color: Color, Font: Font, SelectedCategory: SelectedCategory };
exports.default = AppConstant;
