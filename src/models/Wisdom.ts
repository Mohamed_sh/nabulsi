class Wisdom {
    id?: number;
    cat_id?: number;
    name?: string;
    desc?: string;
    date?: string;
    design_by?: string;
    sources?: string;
    rate?: number;
    image_url?:String

}
export default Wisdom

