class Category {
    id?: number;
    parent_id?: number;
    name?: string;
    name_ar?: string;
    description?: string;
    keywords?: string;
    the_order?: number;

}

export default Category

