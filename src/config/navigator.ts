

import { Component } from "react";
import { Navigation } from "react-native-navigation";
// import * as Navigation from 'react-native-navigation'

import Splash from "../screens/main/Splash";
import MainScreen from "../screens/main/MainScreen";
import SideMenuScreen from "../screens/side-menu/SideMenuScreen";





const registerScreen = () => {


  Navigation.registerComponent(Splash.screenID, () => Splash, );
  Navigation.registerComponent(MainScreen.screenID, () => MainScreen, );
  Navigation.registerComponent(SideMenuScreen.screenID, () => SideMenuScreen, );

};

export default registerScreen;
