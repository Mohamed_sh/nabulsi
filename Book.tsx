import React, { Component } from 'react'
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    FlatList,
    View
} from 'react-native'
import { observer } from "mobx-react";
import Axios from "axios";
const list = [1, 2, 3, 4, 5]


interface Props {
    navigator: any
}

interface State {

}




@observer
export default class Book extends Component<Props, State> {

    async componentDidMount() {
        try {
            const res = await Axios.get("https://developer.github.com/v3/user:email")

        } catch (error) {
            console.log("error -->>", JSON.parse(JSON.stringify(error)));


        }
    }

    renderItem = ({ item, index }) => {
        return (
            <View style={{
                flex: 1,
                height: "15%", marginHorizontal: 10,
                alignSelf: "stretch",
                paddingVertical: 10,
                marginBottom: 10,
                elevation: 5,
                borderWidth: 1,
                borderColor: "green"
            }}>
                <View>
                    <TouchableOpacity style={{ marginTop: -2 }} onPress={() => { }}>
                        <Text> {(item.isFinished) ? `✅` : `🕘`} </Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1 }}>
                    <Text style={{ color: 'black' }}>{item.title}</Text>
                </View>
                <View style={{ justifyContent: 'center' }}>
                    <TouchableOpacity style={{ marginTop: -2 }} onPress={() => { }}>
                        <Text>{`❌`}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {

        return (
            <FlatList
                style={{
                    flex: 1,
                    alignSelf: 'stretch',
                    backgroundColor: 'red',
                    marginTop: 20
                }}
                data={list}
                extraData={list}
                keyExtractor={(item, index) => `${index}`}
                renderItem={this.renderItem}
            />
        )
    }
}