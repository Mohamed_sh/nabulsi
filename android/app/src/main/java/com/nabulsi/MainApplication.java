package com.nabulsi;

import android.app.Application;

import com.facebook.react.ReactApplication;
// import com.airship.customwebview.CustomWebViewPackage;
// import com.christopherdro.RNPrint.RNPrintPackage;
// import com.rumax.reactnative.pdfviewer.PDFViewPackage;
// import com.dylanvann.fastimage.FastImageViewPackage;
// import ui.toasty.RNToastyPackage;
// import com.BV.LinearGradient.LinearGradientPackage;
// import com.github.yamill.orientation.OrientationPackage;
// import com.corbt.keepawake.KCKeepAwakePackage;
// import com.brentvatne.react.ReactVideoPackage;
// import cl.json.RNSharePackage;
// import com.amsoft.RNProgressHUB.RNProgressHUBPackage;
// import com.cmcewen.blurview.BlurViewPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;

import java.util.Arrays;
import java.util.List;
import com.reactnativenavigation.NavigationApplication;


public class MainApplication extends NavigationApplication {

     @Override
     public boolean isDebug() {
         // Make sure you are using BuildConfig from your own application
         return BuildConfig.DEBUG;
     }

     protected List<ReactPackage> getPackages() {
         return Arrays.<ReactPackage>asList(
             new MainReactPackage()
            //  ,
            // new CustomWebViewPackage(),
            // new RNPrintPackage(),
            // new PDFViewPackage(),
            // new FastImageViewPackage(),
            // new RNToastyPackage(),
            // new LinearGradientPackage(),
            // // new OrientationPackage(),
            // // new KCKeepAwakePackage(),
            // // new ReactVideoPackage(),
            // new RNSharePackage(),
            // new RNProgressHUBPackage(),
            // new BlurViewPackage()
         );
     }

     @Override
     public List<ReactPackage> createAdditionalReactPackages() {
         return getPackages();
     }

     @Override
     public String getJSMainModuleName() {
         return "index";
     }


}
