/**
 * @format
 */

import { AppRegistry } from "react-native";
import { Navigation } from "react-native-navigation";
import registerScreen from "./src/config/navigator";
import { name as appName } from "./app.json";
import Splach from "./src/screens/main/Splash";

registerScreen();

Navigation.startSingleScreenApp({
  screen: {
    screen: Splach.screenID
  }
});
