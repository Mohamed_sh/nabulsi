/**
 * Metro configuration for React Native
 * https://github.com/facebook/react-native
 *
 * @format
 */

module.exports = {
  transformer: {
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: false
      }
    })
    // babelTransformerPath: require.resolve("react-native-typescript-transformer") // This is key to using TypeScript instead of Babel for transpiling TS(X) files
  }
};
