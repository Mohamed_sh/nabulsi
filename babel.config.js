module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  plugins: [
    "@babel/plugin-transform-runtime",
    "@babel/plugin-proposal-export-namespace-from",
    ["@babel/plugin-transform-typescript", { allowNamespaces: true }],
    ["@babel/plugin-proposal-decorators", { legacy: true }]
  ]
};
